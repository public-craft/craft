package app.brewery.craft.controller;

import app.brewery.craft.config.CraftConfig;
import app.brewery.craft.config.RabbitMQMessageProducer;
import app.brewery.craft.dto.CreateAppRequest;
import app.brewery.craft.repository.AppComponentRepository;
import app.brewery.craft.repository.AppRepository;
import app.brewery.craft.repository.AppSettingRepository;
import app.brewery.craft.service.AppService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(controllers = AppController.class)
class AppControllerTest {

    @MockBean
    private AppService appService;
    @Autowired
    private MockMvc mockMvc;
    private AppController appController;


    @BeforeEach
    void setup() {
        appController = new AppController(appService);
    }

    @Test
    void shouldCreateApp() throws Exception {
        CreateAppRequest appRequest = CreateAppRequest.builder()
                .name("sample app")
                .build();
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/apps")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(appRequest))
                ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void shouldReturnBadRequestWhenBodyIsInvalid() throws Exception {
        CreateAppRequest appRequest = CreateAppRequest.builder()
                .name("")
                .build();
        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/apps")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(appRequest))
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}