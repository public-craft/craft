package app.brewery.craft.repository;

import app.brewery.craft.model.App;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@ActiveProfiles("test")
class AppRepositoryTest {

    @Autowired
    private AppRepository appRepository;

    @BeforeEach
    void setup() {

    }

    @Test
    void shouldSaveApp() {
        App app = App.builder()
                .name("test app")
                .build();
        App savedApp = appRepository.save(app);
        Assertions.assertThat(savedApp)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(app);
        List<App> apps = appRepository.findAll();
    }

    @Test
    @Sql("classpath:sql/test-data.sql")
    public void shouldFindAllApps() {
        List<App> apps = appRepository.findAll();
        Assertions.assertThat(apps).asList().isNotEmpty();
    }

    @Test
    @Sql("classpath:sql/test-data.sql")
    public void shouldDeleteApp() {
        List<App> apps = appRepository.findAll();
        App app = apps.get(0);
        int id = app.getId();
        appRepository.delete(app);
        Optional<App> deletedApp = appRepository.findById(id);
        Assertions.assertThat(deletedApp.isEmpty()).isTrue();
    }
}