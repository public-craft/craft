package app.brewery.craft.integration;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.spring.CucumberContextConfiguration;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@CucumberContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles("test")
public class AppFeatureTestDefinitions {

    private final static String BASE_URI = "http://localhost";
    private ValidatableResponse validatableResponse;

    @LocalServerPort
    private int port;


    private void configureRestAssured() {
        RestAssured.baseURI = BASE_URI;
        RestAssured.port = port;
    }

    protected RequestSpecification requestSpecification() {
        configureRestAssured();
        return given();
    }

    @Given("I send a GET request to the URL {string} to get all apps")
    public void i_send_a_get_request_to_the_url_to_get_all_apps(String endpoint) {
        validatableResponse = requestSpecification().contentType(ContentType.JSON)
                .when().get(endpoint).then();
        System.out.println("RESPONSE :"+ validatableResponse.extract().asString());
    }

    @Then("the response will return status {int} and data")
    public void the_response_will_return_status_and_data(int status, DataTable table) {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);

        String[] names = rows.stream()
                .map(row -> row.get("name"))
                .collect(Collectors.toList())
                .toArray(String[]::new);

        Integer[] ids = rows.stream()
                .map(row -> Integer.parseInt(row.get("id")))
                .collect(Collectors.toList())
                .toArray(Integer[]::new);

        validatableResponse.assertThat()
                .statusCode(equalTo(status))
                .body("name", containsInRelativeOrder(names))
                .body("id", containsInRelativeOrder(ids));
    }

    @Given("I send a GET request to the URL {string} to get app details")
    public void iSendAGETRequestToTheURLToGetAppDetails(String endpoint) {
        validatableResponse = requestSpecification().contentType(ContentType.JSON)
                .when().get(endpoint).then();
        System.out.println("RESPONSE :"+ validatableResponse.extract().asString());
    }

    @Then("the response will return status {int} and {int} and {string} and {string}")
    public void theResponseWillReturnStatusAndAndAnd(int arg0, int arg1, String arg2, String arg3) {
        validatableResponse.assertThat()
                .statusCode(equalTo(arg0))
                .body("id", equalTo(arg1))
                .body("name", equalTo(arg2))
                .body("appSettingName", equalTo(arg3));
    }
}
