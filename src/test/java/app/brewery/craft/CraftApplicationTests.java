package app.brewery.craft;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class CraftApplicationTests {

	@Test
	void contextLoads() { //NO SONAR
	}

}