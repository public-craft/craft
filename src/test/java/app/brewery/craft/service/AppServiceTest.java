package app.brewery.craft.service;

import app.brewery.craft.config.CraftConfig;
import app.brewery.craft.config.RabbitMQMessageProducer;
import app.brewery.craft.dto.AppResponse;
import app.brewery.craft.dto.CreateAppRequest;
import app.brewery.craft.model.App;
import app.brewery.craft.model.AppComponent;
import app.brewery.craft.model.AppSetting;
import app.brewery.craft.repository.AppComponentRepository;
import app.brewery.craft.repository.AppRepository;
import app.brewery.craft.repository.AppSettingRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AppServiceTest {

    @Mock
    private AppRepository appRepository;
    @Mock
    private AppSettingRepository appSettingRepository;
    @Mock
    private AppComponentRepository appComponentRepository;
    @Mock
    private RabbitMQMessageProducer messageProducer;
    @Mock
    private CraftConfig craftConfig;
    @Captor
    private ArgumentCaptor<AppSetting> appSettingArgumentCaptor;
    @InjectMocks
    private AppService appService;

    @BeforeEach
    void setUp() {
//        appService = new AppService(appRepository, appSettingRepository, appComponentRepository, messageProducer, craftConfig);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    @DisplayName("Should create app")
    void shouldCreateApp() {
        String appName = "new app";
        CreateAppRequest request = CreateAppRequest.builder()
                .name(appName)
                .build();
        App expectedApp = App.builder()
                .name(appName)
                .build();

        Mockito.when(craftConfig.getInternalCraftRoutingKey())
                .thenReturn("test-routing-key");
        Mockito.when(craftConfig.getInternalExchange())
                .thenReturn("test-internal-exchange");
        appService.create(request);
        Mockito.verify(appRepository, Mockito.times(1))
                        .save(ArgumentMatchers.any(App.class));
        Mockito.verify(appComponentRepository, Mockito.times(1))
                .saveAll(ArgumentMatchers.any(List.class));
        Mockito.verify(appSettingRepository, Mockito.times(1))
                .save(ArgumentMatchers.any(AppSetting.class));
        Mockito.verify(messageProducer, Mockito.times(1))
                .publish(
                    ArgumentMatchers.eq("APP_CREATED"),
                    ArgumentMatchers.eq("test-internal-exchange"),
                    ArgumentMatchers.eq("test-routing-key")
                );
        Mockito.verify(appSettingRepository, Mockito.times(1))
                .save(appSettingArgumentCaptor.capture());
        Assertions.assertThat(appSettingArgumentCaptor.getValue().getName().equals(appName + "-setting"));
    }

    @Test
    @DisplayName("Should get all apps")
    void getAll() {
    }

    @Test
    @DisplayName("Should get the app by id if it exist")
    void shouldGetAppById() {
        App app = App.builder()
                .id(1001)
                .name("Sample App")
                .appSetting(AppSetting.builder()
                        .name("sample-app-app-setting")
                        .build())
                .build();
        Mockito.when(appRepository.findById(1001))
                .thenReturn(Optional.of(app));
        AppResponse expected = AppResponse.builder()
                .id(1001)
                .name("Sample App")
                .appSettingName("sample-app-app-setting")
                .build();

        AppResponse actual = appService.getById(1001);
        Assertions.assertThat(actual.equals(expected));
    }

    @Test
    @DisplayName("Should throw Exception if app with specified id does not exist")
    public void shouldThrowExceptionWhenAppNotFound() {
        Mockito.when(appRepository.findById(1001))
                .thenReturn(Optional.ofNullable(null));
        Assertions.assertThatThrownBy(() -> {
            appService.getById(1001);
        }).isInstanceOf(RuntimeException.class)
                .hasMessage("App not found");
    }
}