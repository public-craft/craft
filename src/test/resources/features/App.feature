Feature: Verify springboot application using Cucumber
  Scenario: Send a valid Request to get all apps
    Given I send a GET request to the URL "/api/v1/apps" to get all apps
    Then the response will return status 200 and data
    |id          |name            |appSettingName           |
    |1052        |new-app         |new-app-setting          |
    |1053        |old-app         |old-app-setting          |
    |1054        |mcalacal-app    |mcalacal-app-setting     |

  Scenario Outline: Send a valid Request to get app details
    Given I send a GET request to the URL "/api/v1/apps/<id>" to get app details
    Then the response will return status 200 and <id> and "<name>" and "<appSettingName>"
    Examples:
      |id          |name            |appSettingName           |
      |1052        |new-app         |new-app-setting          |
      |1053        |old-app         |old-app-setting          |
      |1054        |mcalacal-app    |mcalacal-app-setting     |