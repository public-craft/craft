package app.brewery.craft.listener;

import app.brewery.craft.config.CraftConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class CraftMessageListener {

    private final CraftConfig craftConfig;

    @RabbitListener(queues = "#{craftConfig.getCraftQueue()}")
    public void receiveMessage(String message) {
        log.info("Received message: {}", message);

    }
}
