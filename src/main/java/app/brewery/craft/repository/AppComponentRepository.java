package app.brewery.craft.repository;

import app.brewery.craft.model.AppComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppComponentRepository extends JpaRepository<AppComponent, Integer> {
}
