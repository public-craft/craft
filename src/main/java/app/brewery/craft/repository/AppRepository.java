package app.brewery.craft.repository;

import app.brewery.craft.model.App;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRepository extends JpaRepository<App, Integer> {

    @Query(value = "SELECT a from App a LEFT JOIN FETCH a.appSetting", countQuery = "SELECT count(a) FROM App a")
    Page<App> findAll(Pageable pageable);
}
