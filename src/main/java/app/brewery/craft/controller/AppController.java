package app.brewery.craft.controller;

import app.brewery.craft.dto.AppResponse;
import app.brewery.craft.dto.CreateAppRequest;
import app.brewery.craft.service.AppService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1/apps")
@RequiredArgsConstructor
public class AppController {

    private final AppService appService;

    @PostMapping()
    public ResponseEntity<Void> create(@Valid  @RequestBody CreateAppRequest createAppRequest) {

        appService.create(createAppRequest);
        return ResponseEntity.ok(null);
    }

    @GetMapping()
    public ResponseEntity<List<AppResponse>> getAll() {

        List<AppResponse> appResponses = appService.getAll();
        return ResponseEntity.ok(appResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppResponse> getById(@PathVariable("id") Integer id) {

        AppResponse appResponses = null;
        try {
            appResponses = appService.getById(id);
        } catch (RuntimeException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError()
                .body(null);
        }
        return ResponseEntity.ok(appResponses);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
