package app.brewery.craft.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AppResponse {
    private Integer id;
    private String name;
    private String appSettingName;
}
