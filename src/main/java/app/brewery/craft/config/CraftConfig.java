package app.brewery.craft.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class CraftConfig {

    @Value("${rabbitmq.exchanges.internal}")
    private String internalExchange;

    @Value("${rabbitmq.queues.craft}")
    private String craftQueue;

    @Value("${rabbitmq.routing-keys.internal-craft}")
    private String internalCraftRoutingKey;
}
