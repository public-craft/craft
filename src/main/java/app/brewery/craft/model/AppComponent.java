package app.brewery.craft.model;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AppComponent {

    @Id
    @SequenceGenerator(name = "app_component_id_sequence", sequenceName = "app_component_id_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_component_id_sequence")
    private Integer id;
    private String name;
    @ManyToOne()
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    private App app;
}
