package app.brewery.craft.model;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AppSetting {

    @Id
    @SequenceGenerator(name = "app_setting_id_sequence", sequenceName = "app_setting_id_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_setting_id_sequence")
    private Integer id;
    private String name;
    @OneToOne()
    @JoinColumn(name = "app_id", referencedColumnName = "id" )
    private App app;
}
