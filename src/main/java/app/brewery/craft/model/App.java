package app.brewery.craft.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class App {

    @Id
    @SequenceGenerator(name = "app_id_sequence", sequenceName = "app_id_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_id_sequence")
    private Integer id;
    @NotBlank
    private String name;
    @OneToOne(mappedBy = "app")
    @Fetch(FetchMode.JOIN)
    private AppSetting appSetting;

    @OneToMany(mappedBy = "app")
    private List<AppComponent> appComponents;
}
