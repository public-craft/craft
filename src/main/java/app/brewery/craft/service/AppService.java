package app.brewery.craft.service;

import app.brewery.craft.config.CraftConfig;
import app.brewery.craft.config.RabbitMQMessageProducer;
import app.brewery.craft.dto.AppResponse;
import app.brewery.craft.dto.CreateAppRequest;
import app.brewery.craft.model.App;
import app.brewery.craft.model.AppComponent;
import app.brewery.craft.model.AppSetting;
import app.brewery.craft.repository.AppComponentRepository;
import app.brewery.craft.repository.AppRepository;
import app.brewery.craft.repository.AppSettingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AppService {
    private final AppRepository appRepository;
    private final AppSettingRepository appSettingRepository;
    private final AppComponentRepository appComponentRepository;
    private final RabbitMQMessageProducer messageProducer;
    private final CraftConfig craftConfig;

    @Transactional
    public void create(CreateAppRequest createAppRequest) {

        //TODO: DB and JPA config
        App app = App.builder()
                .name(createAppRequest.getName())
                .build();
        AppSetting appSetting = AppSetting.builder()
                .name(createAppRequest.getName() + "-setting")
                .app(app)
                .build();
        AppComponent appComponent1 = AppComponent.builder()
                .name(createAppRequest.getName() + "-component1")
                .app(app)
                .build();
        AppComponent appComponent2 = AppComponent.builder()
                .name(createAppRequest.getName() + "-component2")
                .app(app)
                .build();

        appRepository.save(app);
        appSettingRepository.save(appSetting);
        appComponentRepository.saveAll(List.of(appComponent1, appComponent2));
        messageProducer.publish("APP_CREATED", craftConfig.getInternalExchange(), craftConfig.getInternalCraftRoutingKey());
    }

    public List<AppResponse> getAll() {
        Page<App> apps = appRepository.findAll(PageRequest.of(0, 10));
        return apps.stream()
                .map(app -> AppResponse.builder()
                        .id(app.getId())
                        .name(app.getName())
                        .appSettingName(app.getAppSetting().getName())
                        .build())
                .collect(Collectors.toList());
    }

    public AppResponse getById(Integer id) {
        App app = appRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("App not found"));
        return AppResponse.builder()
                .id(app.getId())
                .name(app.getName())
                .appSettingName(app.getAppSetting().getName())
                .build();
    }
}
