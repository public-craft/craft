-- https://medium.com/@emekadc/how-to-implement-one-to-one-one-to-many-and-many-to-many-relationships-when-designing-a-database-9da2de684710
-- mvn -D"flyway.configFiles=flyway.conf" flyway:migrate
CREATE SEQUENCE app_id_sequence START 1 INCREMENT 50;
CREATE TABLE IF NOT EXISTS app(
    id      INT4 NOT NULL,
    name    VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE SEQUENCE app_setting_id_sequence START 1 INCREMENT 50;
CREATE TABLE IF NOT EXISTS app_setting(
    id      INT4 NOT NULL,
    name    VARCHAR(255) NOT NULL,
    app_id  INT4 UNIQUE NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_app_setting_app
        FOREIGN KEY(app_id)
            REFERENCES app(id)
);

CREATE SEQUENCE app_component_id_sequence START 1 INCREMENT 50;
CREATE TABLE IF NOT EXISTS app_component(
    id      INT4 NOT NULL,
    name    VARCHAR(255) NOT NULL,
    app_id  INT4 NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_app_component_app
        FOREIGN KEY(app_id)
            REFERENCES app(id)
    );
